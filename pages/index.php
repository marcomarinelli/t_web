﻿<!DOCTYPE html>
<html lang="it">
<head>
	<?php 
		require_once("./head.php");
	 ?>

     <link href="../style/cards.css" rel="stylesheet">


     <?php 

        //select all the events --> in future select events with less tickets left

        $sql = 'SELECT * FROM evento';

        //execute the query
        $result = mysqli_query($conn, $sql);

        //fetch the resulting rows as an array
        $events = mysqli_fetch_all($result, MYSQLI_ASSOC);

        //free $result from memory and close connection 
        mysqli_free_result($result);

        mysqli_close($conn);
      ?>
     
     <style type="text/css">
         .carousel-inner {
            min-height: 300px;
            max-height: 400px;
            max-width: auto;
         }
     </style>
</head>
<body>

    <?php 
    	require_once("./templates/header.php");
     ?>

    <!--- Image Slider -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="../imgs/logo/facebook_cover_photo_2.png" class="d-block w-100 " alt="carosello-immagine-evento">
            </div>
           <div class="carousel-item">
                <img src="../imgs/uploaded/calcutta.jpg" class="d-block w-100" alt="carosello-immagine-evento">
            </div>
            <div class="carousel-item">
                <img src="../imgs/uploaded/Teatro_Massimo_Palermo-msu-0462.jpg" class="d-block w-100" alt="carosello-immagine-evento">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="false"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="false"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <!--- Welcome Section -->
    <div class="container-fluid padding">
        <div class="row welcome text-center">
            <div class="col-12">
                <h1 class="display-4">Compra oggi i biglietti per i migliori eventi.</h1>
            </div> <hr>
            <div class="col-12">
                <h2 class="lead"> Un'unica piattaforma, tutti i biglietti per i tuoi eventi preferiti.<br/> Ad un prezzo speciale.</h2>
            </div>
        </div>
    </div>
    <!--- Three Column Section -->
    <div class="container-fluid padding">
        <div class="row text-center padding" >
            <div class="col-xs-12 col-sm-6 col-md-4">
                <i class="fas fa-music" id="icon"></i>
                <h3>Concerti</h3>
                <p>Acquista i biglietti per i concerti dei tuoi artisti preferiti.</p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <i class="fas fa-futbol" id="icon"></i> 
                <h3>Sport</h3>
                <p>Qui trovi i biglietti per gli sport più popolari.</p>
            </div>
            <div class="col-xs-12 col-md-4">
                <i class="fas fa-paint-brush" id="icon"></i>
                <h3>Cultura</h3>
                <p>Compra i biglietti anche per mostre e musei.<br/> Risparmiati la fila!</p>
            </div>
        </div>
    </div>
    
    <!--- Main Events -->
    <div class="container-fluid padding">
        <div class="row welcome text-center">
            <div class="col-12">
                <h1 class="display-4">Eventi più popolari</h1>
            </div>
            <br />
        </div>
    </div>
  
    <!--- Cards for Events-->

         <section class="container-fluid">
    <div class="row">
     <?php foreach($events as $event): ?>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 d-flex">
          <div class="card ">
            <img src="<?php echo htmlspecialchars($event['Immagine']);?>" class="card-img-top img-fluid" alt="immagine-evento">
            <div class="card-body">
              <h4 class="card-title"><?php echo htmlspecialchars($event['Titolo']);?></h4>
              <p class="card-text"><?php echo $event['Ospite']; ?>  |  <?php echo $event['Data']; ?>  |  <?php echo $event['Luogo']; ?> | <?php echo '<b> ' . $event['Costo_biglietto'] . '€</b>' ?></p>              
              <p class="card-text"><?php echo htmlspecialchars($event['Anteprima']);?></p>
            </div>
            <div class="card-footer">
              <a href="event.php?Codice=<?php echo $event['Codice'] ?>" class="btn btn-secondary mr-1 mt-1">Dettagli</a>
              <a href="includes/addToCart.inc.php?Codice=<?php echo $event['Codice'] ?>" class="btn btn-success mr-1 mt-1">Aggiungi <i class="fas fa-shopping-cart"></i></a>
            </div>
        </div> 
       </div>
     <?php endforeach; ?>
    </div>
    </section>

   <?php 
        require_once("./templates/footer.php");
    ?>
</body>

</html>
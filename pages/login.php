<!DOCTYPE html>
<html lang="it">
<head>
   <?php 
        require_once("./head.php");
     ?>
     <link href="../style/login.css" rel="stylesheet">
</head>

<body class="text-center">

    <?php 
        require_once("./templates/header.php");
     ?>

    <!-- Form Login -->
    <form class="form-signin" action="includes/login.inc.php" autocomplete="off" method="POST" >
        <img class="mb-4" src="../imgs/logo/logo.png" alt="" width="92" height="92">
        <legend class="h3 mb-3 font-weight-normal">Login</legend>

        <div class="form-group">
          <input type="text" class="form-control" id="email" name="email" placeholder="Email" autofocus>
          <p id = email-error></p>
        </div>        
        <div class="form-group">
          <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        </div>

        <!--<div class="checkbox mb-3">
            <label>
                <input type="checkbox" value="remember-me"> Ricordami
            </label>
        </div> -->

        <button class="btn btn-lg btn-primary btn-block" type="submit" name='login-submit'>Login</button>
    </form>
    <p>Non hai un account?<a href="./registration.php"> Registrati</a></p>

    <?php 
        require_once("./templates/footer.php");
     ?>

</body>
</html>

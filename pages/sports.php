<!DOCTYPE html>
<html lang="it">
<head>
   <?php 
        require_once("./head.php");
     ?>

     <link href="../style/cards.css" rel="stylesheet">

     <?php 

        //select all the sports 

        $sql = 'SELECT * FROM evento WHERE Categoria = "Sport"';

        //execute the query
        $result = mysqli_query($conn, $sql);

        //fetch the resulting rows as an array
        $sports = mysqli_fetch_all($result, MYSQLI_ASSOC);

        //free $result from memory and close connection 
        mysqli_free_result($result);

        mysqli_close($conn);
      ?>

</head>
<body>

  <?php 
        require_once("./templates/header.php");
     ?>
   
    <!--- Welcome Section -->
    <div class="container-fluid padding">
        <div class="row text-center padding">
            <div class="col-12">
                <i class="fas fa-futbol" id="icon"></i>
                <h1>Tutto il grande Sport</h1>
                <p>Vivi in prima persona i tuoi eventi sportivi preferiti.</p>
            </div>
        </div>
    </div>
  
    <!--- Cards -->

     <section class="container-fluid">
    <div class="row">
     <?php foreach($sports as $sport): ?>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 d-flex">
          <div class="card ">
            <img src="<?php echo htmlspecialchars($sport['Immagine']);?>" class="card-img-top img-fluid"   alt="immagine-evento">
            <div class="card-body">
              <h4 class="card-title"><?php echo htmlspecialchars($sport['Titolo']);?></h4>
              <p class="card-text"><?php echo $sport['Ospite']; ?>  |  <?php echo $sport['Data']; ?>  |  <?php echo $sport['Luogo']; ?> | <?php echo '<b> ' . $sport['Costo_biglietto'] . '€</b>' ?></p>              
              <p class="card-text"><?php echo htmlspecialchars($sport['Anteprima']);?></p>
            </div>
            <div class="card-footer">
              <a href="event.php?Codice=<?php echo $sport['Codice'] ?>" class="btn btn-secondary mr-1 mt-1">Dettagli</a>
              <a href="includes/addToCart.inc.php?Codice=<?php echo $sport['Codice'] ?>" class="btn btn-success mr-1 mt-1">Aggiungi <i class="fas fa-shopping-cart"></i></a>
            </div>
        </div> 
       </div>
     <?php endforeach; ?>
    </div>
    </section>
  
   <?php 
        require_once("./templates/footer.php");
     ?>

</body>

</html>
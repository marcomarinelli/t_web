	<?php 

    session_start();

    if (!isset($_SESSION['cart'])) {
            $_SESSION['cart'] = array();
    }

     ?>
     
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>e-vents</title>

    <link rel="icon" href="../imgs/logo/favicon2.ico">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

        <!-- Custom CSS -->
    <link href="../style/main.css" rel="stylesheet">

    <!--Linked JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

    

    <!--connect to DB -->
    <?php 
        require("dbConnect.php");
     ?>

    
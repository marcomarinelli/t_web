<header>
	<!-- Navigation -->

	<nav class="navbar sticky-top navbar-expand-xl navbar-light bg-light">
	  <a class="navbar-brand" href="index.php"><img src="../imgs/logo/logo_transparent.png" width="60" height="60" alt=""></a>

	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarText">

	    <ul class="navbar-nav mr-auto">

	      			<li class="nav-item">
                        <a class="nav-link" href="./index.php">Home</a>
                    <li class="nav-item">
                        <a class="nav-link" href="./concerts.php">Concerti</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./sports.php">Sport</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./culture.php">Cultura</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./entertainment.php">Spettacolo</a>
                    </li>

                    <?php  
	        		if(isset($_SESSION['user_name'])) {
	        			echo ' <li class="nav-item"><a class="nav-link" href="./includes/logout.inc.php">Logout</a></li>';
		            } else {
		            	echo ' <li class="nav-item"><a class="nav-link" href="./login.php">Login</a></li>';
		            } ?>

		            <li class="nav-item">
                        <a class = "nav-link" href="cart.php">Carrello<i class="fas fa-shopping-cart"></i></a>
                    </li>

				      <?php  
				        if(isset($_SESSION['user_name'])) {
			               echo '<li class="nav-item"><a class = "nav-link" href="myEvents.php">Miei Eventi <i class="fa fa-calendar" aria-hidden="true"></i></a></li>';
			               echo '<li class="nav-item"><a class = "nav-link" href="notifications.php">Notifiche <i class="fa fa-bell" aria-hidden="true"></i></a></li>';
			                if(isset($_SESSION['organizer']) && ($_SESSION['organizer'] === 1)) {
			                	echo '<li class="nav-item"><a class = "nav-link" href="myCreatedEvents.php"> Eventi Creati <i class="fab fa-phabricator"></i></a></li>';
			                }
			                echo '<li class="nav-link disabled" > Benvenuto ' . $_SESSION['user_name'] .' ! </li>';
			            } else {
			            	 echo ' <li class="nav-link disabled"> Benvenuto ospite! </li>';
			            }

			           ?>
	    </ul>


	    <?php 
			if(isset($_SESSION['organizer']) && ($_SESSION['organizer'] === 1)) {
				echo '<a href="createEvent.php" class="btn btn-success nav-item my-2 my-sm-2 float-right">Crea Evento <i class="fas fa-plus"></i></a>';
			}

		?>


	  </div>
	</nav>
</header>
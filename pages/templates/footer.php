<!--- Footer -->

    <footer>
        <div class="container-fluid padding">
            <div class="row text-center padding">
                <div class="col-md-4">
                    <img src="../imgs/logo/logo_transparent.png" style="max-height: 180px; max-width: 180px;">    
                </div>
                <div class="col-md-4">
                    <h4>Contatti</h4>
                    <p>marco.marinelli4@studio.unibo.it</p>
                    <p>Via dell'Università 1, Cesena, FC</p>
                    <p>Progetto di Tecnologie Web © </p>  
                     <a href="../pages/includes/promotion.inc.php">Diventa organizzatore di eventi!</a>                
                </div>
                <div class="col-md-4">
                    <div class="social padding">
                        <h4>Social</h4>
                        <a href="#"><i class="fab fa-facebook"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-youtube"></i></a>

                    </div>
                </div>
            </div>
         </div>     
    </footer>
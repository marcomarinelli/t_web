<!DOCTYPE html>
<html lang="it">
<head>

  <?php 
    require_once("./head.php");
     require_once("./dbConnect.php");
  ?>


<?php 
  
  if (!empty($_SESSION['cart'])) {

    $whereIn = implode(',', $_SESSION['cart']);

    $sql = "SELECT * FROM evento WHERE Codice IN ($whereIn)";

    //execute the query
    $result = mysqli_query($conn, $sql);

    //fetch the resulting rows as an array
    $carts = mysqli_fetch_all($result, MYSQLI_ASSOC);

    //free $result from memory and close connection 
    mysqli_free_result($result);

    mysqli_close($conn);
  }

 

?>

</head>

<body>
    
    <?php 
        require_once("./templates/header.php");
    ?>

    <?php  if(empty($_SESSION['cart'])): ?>
      <div class= "container" style = "min-height: 400px;">
            <div class="row">
                <div class="col-12" style ="text-align: center; padding-top: 70px;">
                    <h2>Il tuo carrello è vuoto :(</h2>
                    <h3>Scegli tra: </h3><br>
                    <ul style="text-align: left;">
                      <li><a href="concerts.php" style="font-size: larger;">Concerti</a></li>
                      <li><a href="sports.php" style="font-size: larger;">Eventi Sportivi</a></li>
                      <li><a href="culture.php" style="font-size: larger;">Eventi Culturali</a></li>
                      <li><a href="entertaiment.php" style="font-size: larger;">Spettacolo</a></li>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif ?>

<?php if(!empty($_SESSION['cart'])): ?>

  <div class="pb-5" style="margin-top: 50px;">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 p-5 bg-light rounded shadow mb-5">

          <!-- Shopping cart table -->
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr class="rounded shadow-sm">
                  <th scope="col" class="border-0 bg-white ">
                    <div class="p-2 px-3 text-uppercase">Evento</div>
                  </th>
                  <th scope="col" class="border-0 bg-white">
                    <div class="py-2 text-uppercase">Prezzo</div>
                  </th>
                  <th scope="col" class="border-0 bg-white">
                    <div class="py-2 text-uppercase">Quantità</div>
                  </th>
                  <th scope="col" class="border-0 bg-white">
                    <div class="py-2 text-uppercase">Rimuovi</div>
                  </th>
                </tr>
              </thead>
              <tbody>
               <?php foreach($carts as $cart): ?>
                <tr>
                  <th scope="row" class="border-0">
                    <div class="p-2">
                      <img src="<?php echo htmlspecialchars($cart['Immagine']);?>" alt="" width="70" class="img-fluid rounded shadow-sm">
                      <div class="ml-3 d-inline-block align-middle">
                        <h5 class="mb-0"> <?php echo htmlspecialchars($cart['Titolo']);?></h5><span class="text-muted font-weight-normal d-block"><?php echo $cart['Ospite']; ?>  |  <?php echo $cart['Data']; ?>  |  <?php echo $cart['Luogo']; ?></span>
                      </div>
                    </div>
                  </th>
                  <td class="border-0 align-middle"><strong><?php echo $cart['Costo_biglietto'] . '€' ?></strong></td>
                  <td class="border-0 align-middle"><strong>1</strong></td>
                  <td class="border-0 align-middle"><a href="includes/removeFromCart.inc.php?Codice=<?php echo $cart['Codice'] ?>" class="text-dark"><i class="fa fa-trash"></i></a></td>
                </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
          <!-- End -->
        </div>
      </div>

      <div class="row py-5 p-4 bg-light rounded shadow">
        <div class="col">
          <div class="bg-white shadow-sm rounded-pill px-4 py-3 text-uppercase font-weight-bold">Riepilogo Ordine</div>
          <div class="p-4">
            <p class="font-italic mb-4">Riceverai una ricevuta del tuo acquisto direttamente nella casella di posta elettronica indicata in fase di registrazione.</p>
            <ul class="list-unstyled mb-4">
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Totale</strong>
                <h5 class="font-weight-bold"> 
                  <?php 

                    $tot = 0;

                    foreach($carts as $cart) {
                      $tot = $tot + $cart['Costo_biglietto'];
                    }
                    echo $tot;
                   ?>
                  €</h5>
              </li>
            </ul>
            <?php 
              if(isset($_SESSION['user_name'])) {
                echo'<a href="checkout.php" class="btn btn-dark rounded-pill py-2 btn-block">Checkout</a>';
              } else {
                echo '<a href="login.php" class="btn btn-dark rounded-pill py-2 btn-block">Effettua il login prima di procedere!</a>';
              }
            ?>
           
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

    <?php endif ?> 
    
    <?php 
        require_once("./templates/footer.php");
    ?>
    
  </body>

</html>
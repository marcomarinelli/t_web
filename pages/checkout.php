<!DOCTYPE html>
<html lang="it">
<head>

  <?php 
    require_once("./head.php");
     require_once("./dbConnect.php");
  ?>

  <style type="text/css">
  	.container {
 	 max-width: 960px;
 	 margin-top: 40px;
	}
	form {
        text-align: left;
      }
  </style>
 
</head>

<body class="bg-light">

	<?php 
        require_once("./templates/header.php");
    ?>

    <?php 
    	if(!isset($_SESSION['user_name'])) {
    		header('Location: index.php');
    		exit();
    	}
     ?>

     
    <div class="container" align="center">
      <div class="row">
        <div class="col-md-12 order-md-1">
          <h4 class="mb-3" align="left">Indirizzo di Fatturazione</h4>
          <form class="needs-validation" action="includes/checkout.inc.php" method="POST" novalidate autocomplete="off">
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">Nome</label>
                <input type="text" class="form-control" id="firstName" name="name" placeholder="" value="" required>
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Cognome</label>
                <input type="text" class="form-control" id="lastName" name="surname"placeholder="" value="" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
            </div>

            <div class="mb-3">
              <label for="address">Indirizzo</label>
              <input type="text" class="form-control" id="address" name="address"placeholder="es: Via Petrarca 25" required>
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <hr class="mb-4">

            <h4 class="mb-3">Metodo di Pagamento</h4>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="credit" name="payment-method" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="credit">Carta</label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="cc-name">Titolare Carta</label>
                <input type="text" class="form-control" id="cc-name" name="cc-name"placeholder="" required>
                <small class="text-muted">Nome e cognome che compaiono sulla carta</small>
                <div class="invalid-feedback">
                  Name on card is required
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="cc-number">Numero Carta</label>
                <input type="text" class="form-control" id="cc-number" name="cc-number" placeholder="" required>
                <div class="invalid-feedback">
                  Credit card number is required
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 mb-3">
                <label for="cc-expiration">Scadenza</label>
                <input type="text" class="form-control" id="cc-expiration" name="cc-expiration" placeholder="" required>
                <div class="invalid-feedback">
                  Expiration date required
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <label for="cc-cvv">CVV</label>
                <input type="text" class="form-control" id="cc-cvv" name="cc-cvv" placeholder="" required>
                <div class="invalid-feedback">
                  Security code required
                </div>
              </div>
            </div>
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit" name="checkout">Acquista</button>
          </form>
        </div>
      </div>
    </div>
 
    <?php 
        require_once("./templates/footer.php");
    ?>
    
  </body>

</html>
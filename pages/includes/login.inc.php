<?php 
	if(isset($_POST['login-submit'])) {
		//mysqli_real_escape_string() to prevent SQL injection

		require '../dbConnect.php';

        $email = mysqli_real_escape_string($conn,$_POST['email']);
        $password = mysqli_real_escape_string($conn,$_POST['password']);

        if(empty($email) || empty($password)) {

            header("Location: ../login.php?error=emptyfields");
 
        } else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            header("Location: ../login.php?error=invalidemail");
            exit();
        } else {

        	$sql = "SELECT * FROM utente WHERE Email=?";
        	$stmt = mysqli_stmt_init($conn);

        	if (!mysqli_stmt_prepare($stmt, $sql)) {
        		header("Location: ../login.php?error=sqlerror");
             	exit();
        	} else {

        		mysqli_stmt_bind_param($stmt, "s", $email);
             	mysqli_stmt_execute($stmt);
             	$result = mysqli_stmt_get_result($stmt);
             	
             	if($row = mysqli_fetch_assoc($result)) {
             		$passCheck = password_verify($password, $row['Password']);

             		if($passCheck == false) {
             			header("Location: ../login.php?error=wrongpassword");
             			exit();
             		} else if ($passCheck == true) {

             			session_start();
             			$_SESSION['user_id'] = $row['ID'];
             			$_SESSION['user_name'] = $row['Nome'];
             			$_SESSION['user_surname'] = $row['Cognome'];
                        $_SESSION['organizer'] = $row['Organizzatore'];
                        $_SESSION['admin'] = $row['Admin'];
             			header("Location: ../index.php?login=success");
             			exit();
             		}

             	} else {
             		header("Location: ../login.php?error=noemailindb");
             		exit();
             	}

        	}
        }
    } 
    else {
    	header("Location: ../login.php#");
    }
 ?>

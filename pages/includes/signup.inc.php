<?php 
	if(isset($_POST['save-inputs'])) {

            //mysqli_real_escape_string() to prevent SQL injection

		require '../dbConnect.php';

            $name = mysqli_real_escape_string($conn,$_POST['name']);
            $surname = mysqli_real_escape_string($conn,$_POST['surname']);
            $email = mysqli_real_escape_string($conn,$_POST['email']);
            $password = mysqli_real_escape_string($conn,$_POST['password']);
            $passwordRepeat = mysqli_real_escape_string($conn,$_POST['password-repeat']);


             if(empty($name) || empty($surname) || empty($email) || empty($password) || empty($passwordRepeat) ) {
             	header("Location: ../registration.php?error=emptyfields");
             	exit();
             } else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
             	header("Location: ../registration.php?error=invalidemail");
             	exit();
             } else if(!preg_match("/^[a-zA-Z]*$/", $name) || !preg_match("/^[a-zA-Z]*$/", $surname)) {
             	header("Location: ../registration.php?error=invalidpersonaldata ");
             	exit();
             } else if ($password !== $passwordRepeat) {
             	header("Location: ../registration.php?error=passwordnotmatching");
             	exit();
             } else {

             	$sql = "SELECT Email FROM utente WHERE Email=?";
             	$stmt = mysqli_stmt_init($conn);

             	if(!mysqli_stmt_prepare($stmt, $sql)) {
             		header("Location: ../registration.php?error=sqlerror");
             		exit();
             	} else {
             		mysqli_stmt_bind_param($stmt, "s", $email);
             		mysqli_stmt_execute($stmt);
             		mysqli_stmt_store_result($stmt);
             		$resultCheck = mysqli_stmt_num_rows($stmt);

             		if($resultCheck > 0) {
             			header("Location: ../registration.php?error=alreadyregistered");
             			exit();
             		}
             		else {
             			 // create query
            			$sql = "INSERT INTO utente(nome,cognome,email,password) VALUES(?, ?, ?, ?)";	
            			$stmt = mysqli_stmt_init($conn);

            			if(!mysqli_stmt_prepare($stmt, $sql)) {
            				header("Location: ../registration.php?error=sqlinserterror");
             				exit();
            			}
            			else {

            				//hashing password before sending using bcrypt
            				$hashedPassword = password_hash($password, PASSWORD_DEFAULT);

            				mysqli_stmt_bind_param($stmt, "ssss", $name, $surname, $email, $hashedPassword);
		             		mysqli_stmt_execute($stmt);
		             		header("Location: ../login.php?signup=success");
             				exit();
            			}
             		}
             	}
             }

        mysqli_stmt_close($stmt);
        mysqli_close($conn);
    } else {
    	header("Location: ../registration.php");
    }
 ?>
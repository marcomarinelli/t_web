<?php 

      session_start();

	if(isset($_POST['checkout'])) {

            //mysqli_real_escape_string() to prevent SQL injection

		require '../dbConnect.php';

            $purchases = $_SESSION['cart'];

            $name = mysqli_real_escape_string($conn,$_POST['name']);
            $surname = mysqli_real_escape_string($conn,$_POST['surname']);
            $address = mysqli_real_escape_string($conn,$_POST['address']);
            $ccName = mysqli_real_escape_string($conn,$_POST['cc-name']);
            $ccNumber = mysqli_real_escape_string($conn,$_POST['cc-number']);
            $ccExpiration = mysqli_real_escape_string($conn,$_POST['cc-expiration']);
            $ccCvv = mysqli_real_escape_string($conn,$_POST['cc-cvv']);


             if(empty($name) || empty($surname) || empty($address) || empty($ccName) || empty($ccNumber) || empty($ccExpiration) || empty($ccCvv)) {
             	header("Location: ../registration.php?error=emptyfields");
             	exit();
            } else if(!preg_match("/^[a-zA-Z]*$/", $name) || !preg_match("/^[a-zA-Z]*$/", $surname)) {
             	header("Location: ../registration.php?error=invalidpersonaldata ");
             	exit();
             } else if (!preg_match("/^[0-9]*$/", $ccNumber)  || !preg_match("/^[0-9]*$/", $ccCvv)) {
             	header("Location: ../registration.php?error=invalidccdata");
             	exit();
             } else {

                  
            foreach ($purchases as $purchase) {


                  // create query
                  $sql = "INSERT INTO fattura(Cod_cliente,Nome,Cognome,Indirizzo, Titolare_carta, Numero_carta, Scadenza, CVV, Cod_evento) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)"; 

                  $stmt = mysqli_stmt_init($conn);

                  if(!mysqli_stmt_prepare($stmt, $sql)) {
                        header("Location: ../cart.php?error=sqlinserterror");
                        exit();
                  }
                  else {

                        mysqli_stmt_bind_param($stmt, "issssisii", $_SESSION['user_id'], $name, $surname, $address, $ccName, $ccNumber, $ccExpiration, $ccCvv, $purchase);
                        mysqli_stmt_execute($stmt);

                        if(!$stmt) {
                              header("Location: ../index.php?purchase=failed");
                              exit();
                        }
                  }


            }

            foreach ($purchases as $purchase) {

              $sql = "UPDATE evento SET B_venduti = B_venduti + 1 , B_disponibili = B_disponibili - 1  WHERE Codice = $purchase";

              $result = mysqli_query($conn, $sql);

              if(!$result) {
                echo 'fail';
                exit();
              }

            }

      }

      mysqli_stmt_close($stmt);
      mysqli_close($conn);

      unset($_SESSION['cart']);
      echo 'Acquisto andato a buon fine! Verrai reindirizzato alla home page in 3 secondi!';
      header( "refresh:3; url= ../index.php" );
      exit();
        
    } else {
    	header("Location: ../index.php");
    }
 ?>
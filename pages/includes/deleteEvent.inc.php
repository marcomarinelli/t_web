<?php 
	session_start();

	if(isset($_POST['delete'])) {

		require '../dbConnect.php';

		$id = mysqli_real_escape_string($conn, $_GET['Codice']);

		$sql =  "DELETE FROM `evento` WHERE `evento`.`Codice` = $id";

		$result = mysqli_query($conn, $sql);

        mysqli_close($conn);


        echo 'Evento eliminato correttamente! Verrai reindirizzato alla home page in 3 secondi!';
      	header( "refresh:3; url= ../index.php" );
        exit();


	} else {
    	header("Location: #");
    	exit();
    }
 ?>
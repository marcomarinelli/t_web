<!DOCTYPE html>
<html lang="it">
<head>

   <?php 
        require_once("./head.php");
     ?>

     <link href="../style/cards.css" rel="stylesheet">

     <?php 

        //select all id of the events bought by the user, from the billing table

        $uid = $_SESSION['user_id'];

        $sql = "SELECT Cod_evento FROM fattura WHERE Cod_cliente = '$uid' ";

        //execute the query
        $result = mysqli_query($conn, $sql);

        //fetch the resulting rows as an array
        $event_ids = mysqli_fetch_all($result, MYSQLI_ASSOC);


        $purchased = array();

        foreach ($event_ids as $event_id) {

          $c_evento = $event_id['Cod_evento'];

          $query = "SELECT * FROM evento WHERE Codice = '$c_evento' ";
          $result = mysqli_query($conn, $query);
          $resultCheck = mysqli_num_rows($result);
          if($resultCheck > 0) {
            $result_assoc = mysqli_fetch_all($result, MYSQLI_ASSOC);
            array_push($purchased, $result_assoc);
          }
          
        }
        

        //free $result from memory and close connection 
        mysqli_free_result($result);
        mysqli_close($conn);

      ?>

</head>
<body>

  <?php 
        require_once("./templates/header.php");
     ?>
   
    <!--- Welcome Section -->
    <div class="container-fluid padding">
        <div class="row text-center padding">
            <div class="col-12">
                <i class="fa fa-calendar" aria-hidden="true" id="icon"></i>
                <h1>I Miei Eventi</h1>
                <p>Cronologia dei tuoi eventi acquistati.</p>
            </div>
        </div>
    </div>


  
    <!--- Cards -->

    <section class="container-fluid" style = "min-height: 400px;">
    <div class="row">
     <?php foreach($purchased as $purchase): ?>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 d-flex">
          <div class="card ">
            <img src="<?php echo htmlspecialchars($purchase[0]['Immagine']);?>" class="card-img-top img-fluid" alt="immagine-evento">
            <div class="card-body">
              <h4 class="card-title"><?php echo htmlspecialchars($purchase[0]['Titolo']);?></h4>
              <p class="card-text"><?php echo $purchase[0]['Ospite']; ?>  |  <?php echo $purchase[0]['Data']; ?>  |  <?php echo $purchase[0]['Luogo']; ?> | <?php echo '<b> ' . $purchase[0]['Costo_biglietto'] . '€</b>' ?></p>              
              <p class="card-text"><?php echo htmlspecialchars($purchase[0]['Anteprima']);?></p>
            </div>
            <div class="card-footer">
              <a href="event.php?Codice=<?php echo $purchase[0]['Codice'] ?>" class="btn btn-secondary mr-1 mt-1">Dettagli</a>
            </div>
        </div> 
       </div>
     <?php endforeach; ?>
    </div>
    </section>

   <?php 
        require_once("./templates/footer.php");
     ?>

</body>

</html>
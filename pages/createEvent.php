<!DOCTYPE html>
<html lang="it">
<head>
    <?php 
        require_once("./head.php");
     ?>

     <style type="text/css">
         .error {
          color: red;
          font-size: small;
          padding-left: 5px;
          padding-top: 3px;
          font-style: italic;
      }

      form {
        text-align: left;
      }


     </style>

</head>

<body>

   <?php 
        require_once("./templates/header.php");
     ?>


     <?php 
     	if(!isset($_SESSION['organizer']) || $_SESSION['organizer'] <> 1) {
     		header('Location: index.php');
     		exit();
     	}
      ?>


<!--Section: Contact v.2-->
<div class="container-fluid mb-4" align="center">

    <!--Section heading-->
    <h2 class="h1-responsive font-weight-bold text-center my-4">Creazione evento</h2>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5">Compila tutti i campi senza lasciarne nessuno vuoto. Ricorda: inserisci solo numeri nei campi Biglietti disponibili e Costo unitario biglietto e solo testo in tutti gli altri (no numeri).</p>


<form  action = "createEvent.inc.php" method="POST" id="registrationForm" enctype="multipart/form-data" autocomplete="off" style="margin:30px; max-width: 800px;">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="title">Titolo Evento</label>
      <input type="text" class="form-control" name="title" placeholder="Titolo">
    </div>
    <div class="form-group col-md-6">
      <label for="guest">Ospite</label>
      <input type="text" class="form-control" name="guest" placeholder="Ospite">
    </div>
  </div>
  <div class="form-group">
    <label for="place">Località</label>
    <input type="text" class="form-control" name="place" placeholder="Località">
  </div>
  <div class="form-group">
      <label for="category">Categoria</label>
      <select name="category" class="form-control">
        <option selected>Concerto</option>
        <option>Sport</option>
        <option>Cultura</option>
        <option>Spettacolo</option>
      </select>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="date">Data</label>
      <input type="date" class="form-control" name="date" placeholder="">
    </div>
    <div class="form-group col-md-4">
      <label for="tickets">Biglietti disponibili</label>
      <input type="text" class="form-control" name="tickets" >
    </div>
    <div class="form-group col-md-4">
      <label for="t_price">Costo unitario biglietto</label>
      <input type="text" class="form-control" name="t_price" >
    </div>
  </div>
  <div class="form-row">
  <div class="form-group col-12">
      <label for="preview">Anteprima</label>
      <textarea type="text" class="form-control" name="preview" placeholder="Inserisci una breve descrizione dell'evento (max 140 caratteri)"></textarea>
    </div>
   </div>
  <div class="form-row">
    <div class="form-group col-12">
      <label for="desc">Descrizione</label>
      <textarea type="text" class="form-control" name="desc" placeholder="Inserisci la descrizione che verrà visualizzata nella pagina evento - max 1000 caratteri"></textarea>
     </div>
  </div>
  <div class="form-row">
     <div class="form-group col-12">
      <label for="file">Immagine Evento</label>
      <input type="file" class="form-control" name="file">
     </div>
  </div>
 
  <button type="submit" class="btn btn-primary btn-lg" name="create-event">Invia</button>
</form>

</div>


   <?php 
        require_once("./templates/footer.php");
     ?>

</body>
    

</html>

var formValid = {
  name: false,
  surname: false,
  email: false,
};

function checkValidation() {
  if(formValid.name && formValid.surname && formValid.email) {
    $('#save-inputs').removeAttr('disabled');
  } else {
    $('#save-inputs').attr('disabled', true);
  }
}


$('#name').on('input', function() {
  var name = $(this).val();

  if(name.length < 1) {
    $('#name-error').text('Campo obbligatorio!').show();
    formValid.name = false;
    checkValidation();
  } else {
    $('#name-error').hide();
    formValid.name = true;
    checkValidation();
  }
});

$('#surname').on('input', function() {
  var surname = $(this).val();

  if(surname.length < 1) {
    $('#surname-error').text('Campo obbligatorio!').show();
    formValid.surname = false;
    checkValidation();
  } else {
    $('#surname-error').hide();
    formValid.surname = true;
    checkValidation();
  }
});


$('#email').on('input', function() {
  var email = $(this).val();

  if(email.length < 1) {
    $('#email-error').text('Campo obbligatorio!').show();
    formValid.email = false;
    checkValidation();
  } else {
    $('#email-error').hide();
    formValid.email = true;
    checkValidation();
  }
});

<?php 

define('UPLOAD_DIR', '../imgs/uploaded/');
chmod(UPLOAD_DIR, 777); 

function uploadImage($path, $image){
    $imageName = basename($image["name"]);
    $fullPath = $path.$imageName;
    
    $maxKB = 50000;
    $acceptedExtensions = array("jpg", "jpeg", "png", "gif");
    $result = 0;
    $msg = "";
    //Controllo se immagine è veramente un'immagine
    $imageSize = getimagesize($image["tmp_name"]);
    if($imageSize === false) {
        $msg .= "File caricato non è un'immagine! ";
    }
    //Controllo dimensione dell'immagine 
    if ($image["size"] > $maxKB * 1024) {
        $msg .= "File caricato pesa troppo! Dimensione massima è $maxKB KB. ";
    }

    //Controllo estensione del file
    $imageFileType = strtolower(pathinfo($fullPath,PATHINFO_EXTENSION));
    if(!in_array($imageFileType, $acceptedExtensions)){
        $msg .= "Accettate solo le seguenti estensioni: ".implode(",", $acceptedExtensions);
    }

    //Controllo se esiste file con stesso nome ed eventualmente lo rinomino
    if (file_exists($fullPath)) {
        $i = 1;
        do{
            $i++;
            $imageName = pathinfo(basename($image["name"]), PATHINFO_FILENAME)."_$i.".$imageFileType;
        }
        while(file_exists($path.$imageName));
        $fullPath = $path.$imageName;
    }

    //Se non ci sono errori, sposto il file dalla posizione temporanea alla cartella di destinazione
    if(strlen($msg)==0){
        if(!move_uploaded_file($image["tmp_name"], $fullPath)){
            $msg.= "Errore nel caricamento dell'immagine.";
        }
        else{
            $result = 1;
            $msg = $imageName;
        }
    }
    return array($result, $msg);
}

      
     session_start();
      
	if(isset($_POST['create-event'])) {

            $uid = $_SESSION['user_id'];

            //mysqli_real_escape_string() to prevent SQL injection

		require 'dbConnect.php';

            $title = mysqli_real_escape_string($conn,$_POST['title']);
            $guest = mysqli_real_escape_string($conn,$_POST['guest']);
            $place = mysqli_real_escape_string($conn,$_POST['place']);
            $category = mysqli_real_escape_string($conn,$_POST['category']);
            $date = mysqli_real_escape_string($conn,$_POST['date']);
            $tickets = mysqli_real_escape_string($conn,$_POST['tickets']);
            $t_price = mysqli_real_escape_string($conn,$_POST['t_price']);
            $preview = mysqli_real_escape_string($conn,$_POST['preview']);
            $desc = mysqli_real_escape_string($conn,$_POST['desc']);


             if(empty($title) || empty($guest) || empty($place) || empty($tickets) || empty($t_price) || empty($preview) || empty($desc)) {
             	header("Location: createEvent.php?error=emptyfields");
             	exit();
             } else if (!preg_match("/^[0-9]*$/", $tickets) || !preg_match("/^[0-9]*$/", $t_price)) {
             	header("Location: createEvent.php?error=invalidticketfield");
             	exit();
             } else {

                  
                  list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["file"]);
                  if($result != 0){

                        $imgarticolo = UPLOAD_DIR.$msg;

                   	 // create query
                  	$sql = "INSERT INTO evento(Titolo,Descrizione,Data,Luogo,B_disponibili,Categoria,Ospite,Anteprima,Costo_biglietto,Codice_creatore, Immagine) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";	

                  	$stmt = mysqli_stmt_init($conn);

                  	if(!mysqli_stmt_prepare($stmt, $sql)) {

                  		header("Location: createEvent.php?error=sqlinserterror");
                   		exit();
                  	}
                  	else {

                  		mysqli_stmt_bind_param($stmt, "ssssisssiis", $title, $desc, $date, $place, $tickets, $category, $guest, $preview, $t_price, $uid, $imgarticolo);

      		          if(mysqli_stmt_execute($stmt)) {

                              echo 'Evento creato correttamente ed inserito nel db! In 3 secondi sarai rediretto.';
                              header( "refresh:3; url= myCreatedEvents.php" );
                              exit();
                              }
      		             
                  	}
             		
                  }
                   
            }

           

      } else {
            header("Location: createEvent.php");
            exit();
      }

 ?>
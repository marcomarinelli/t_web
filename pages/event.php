﻿<!DOCTYPE html>
<html lang="it">
<head>
    <?php 
        require_once("./head.php");
     ?>

     <?php 
        //check GET request id parameter
        if(isset($_GET['Codice'])) {

            $id = mysqli_real_escape_string($conn, $_GET['Codice']);

            $sql = "SELECT * FROM evento WHERE Codice = $id";

            $result = mysqli_query($conn, $sql);

            $event = mysqli_fetch_assoc($result);

            mysqli_free_result($result);
            mysqli_close($conn);
        }

        $c = $_GET['Codice'];
      ?>

</head>

<body>
    
    <?php 
        require_once("./templates/header.php");
    ?>



<!--- Cards -->
          
<?php if($event): ?>

    <div class="container-fluid padding">
        <div class="row padding">
            <div class="col-sm-12 col-md-6 align-items-center" style="padding: 30px;" >

                <div class="card" style="max-height: 550px; max-width: 550px; " >
                    <img class="card-img-top img-fluid" src="<?php echo htmlspecialchars($event['Immagine']);?>" >
                </div>
            </div>

                <div class="col-sm-12 col-md-6 align-items-center" style="padding: 40px;">
                	<h4><?php echo $event['Titolo']; ?></h4>
                	<br> <p class="card-text"> <?php echo $event['Ospite']; ?>  |  <?php echo $event['Data']; ?>  |  <?php echo $event['Luogo']; ?> | <?php echo '<b> ' . $event['Costo_biglietto'] . '€</b>' ?></p>
                    <p><?php echo $event['Descrizione']; ?></p>

                    <a href="./includes/addToCart.inc.php?Codice=<?php echo $event['Codice'] ?>" class="btn btn-success mr-1 mt-3" style="color: white;">Aggiungi al Carrello <i class="fas fa-shopping-cart"></i></a>


                    <?php if(isset($_SESSION['organizer']) && $_SESSION['organizer'] === 1): ?>


                        <?php if ($_SESSION['user_id'] == $event['Codice_creatore'] ): ?>

                            <form class="form" style="float: center;" method="POST" action="includes/deleteEvent.inc.php?Codice=<?php echo $event['Codice'] ?>" > 
                                <button type="submit" name="delete" class="btn btn-danger mr-1 mt-1">Elimina <i class="fa fa-trash"></i></button>
                            </form>
                        <?php endif ?>
                    <?php endif ?>
                </div>
        </div> 
    </div>  
               

<?php else: ?>
        <div class= "container" style = "min-height: 400px;">
            <div class="row">
                <div class="col-12" style ="text-align: center; padding-top: 70px;">
                    <h2>Ehy! Questo evento non esiste!</h2>
                    <br>
                    <a href="index.php" style="min-height: 100px;">
                      Torna alla pagina principale
                    </a>
                </div>
            </div>
        </div>
<?php endif ?>  
            
    </div>   
</div> 

    
    <?php 
        require_once("./templates/footer.php");
    ?>
    
</body>

</html>
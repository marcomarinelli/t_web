<!DOCTYPE html>
<html lang="it">
<head>
   <?php 
        require_once("./head.php");
     ?>

     <link href="../style/cards.css" rel="stylesheet">

    <?php 

        //select all the cultural events 

        $sql = 'SELECT * FROM evento WHERE Categoria = "cultura"';

        //execute the query
        $result = mysqli_query($conn, $sql);

        //fetch the resulting rows as an array
        $cultures = mysqli_fetch_all($result, MYSQLI_ASSOC);

        //free $result from memory and close connection 
        mysqli_free_result($result);

        mysqli_close($conn);
      ?>

     <link href="../style/categories.css" rel="stylesheet">
     <link href="../style/card.css" rel="stylesheet">

</head>
<body>

  <?php 
        require_once("./templates/header.php");
     ?>
   
    <!--- Welcome Section -->
    <div class="container-fluid padding">
        <div class="row text-center padding">
            <div class="col-12">
                <i class="fas fa-paint-brush" id="icon"></i>
                <h1>Biglietti per musei e mostre</h1>
                <p>Acquista i biglietti per le esposizioni e i musei più belli del mondo.</p>
            </div>
        </div>
    </div>
  
    <!--- Cards -->
   
     <section class="container-fluid">
    <div class="row">
     <?php foreach($cultures as $culture): ?>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 d-flex">
          <div class="card ">
            <img src="<?php echo htmlspecialchars($culture['Immagine']);?>" class="card-img-top img-fluid"   alt="immagine-evento">
            <div class="card-body">
              <h4 class="card-title"><?php echo htmlspecialchars($culture['Titolo']);?></h4>
              <p class="card-text"><?php echo $culture['Ospite']; ?>  |  <?php echo $culture['Data']; ?>  |  <?php echo $culture['Luogo']; ?> | <?php echo '<b> ' . $culture['Costo_biglietto'] . '€</b>' ?></p>              
              <p class="card-text"><?php echo htmlspecialchars($culture['Anteprima']);?></p>
            </div>
            <div class="card-footer">
              <a href="event.php?Codice=<?php echo $culture['Codice'] ?>" class="btn btn-secondary mr-1 mt-1">Dettagli</a>
              <a href="includes/addToCart.inc.php?Codice=<?php echo $culture['Codice'] ?>" class="btn btn-success mr-1 mt-1">Aggiungi <i class="fas fa-shopping-cart"></i></a>
            </div>
        </div> 
       </div>
     <?php endforeach; ?>
    </div>
    </section>
            

   <?php 
        require_once("./templates/footer.php");
     ?>

</body>

</html>
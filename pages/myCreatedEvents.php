<!DOCTYPE html>
<html lang="it">
<head>

   <?php 
        require_once("./head.php");
     ?>

     <link href="../style/cards.css" rel="stylesheet">

     <?php 

        //select all the events created by the user-organizer

        $uid = $_SESSION['user_id'];

        $sql = "SELECT * FROM evento WHERE Codice_creatore = '$uid' ";

        //execute the query
        $result = mysqli_query($conn, $sql);

        //fetch the resulting rows as an array
        $created = mysqli_fetch_all($result, MYSQLI_ASSOC);

        //free $result from memory and close connection 
        mysqli_free_result($result);

        mysqli_close($conn);

      ?>

</head>
<body>

  <?php 
        require_once("./templates/header.php");
     ?>
   
    <!--- Welcome Section -->
    <div class="container-fluid padding">
        <div class="row text-center padding">
            <div class="col-12">
                <i class="fab fa-phabricator" id="icon"></i>
                <h1>Eventi Creati</h1>
                <p>Cronologia degli eventi creati da te.</p>
            </div>
        </div>
    </div>


  
    <!--- Cards -->

    <section class="container-fluid" style = "min-height: 400px;">
    <div class="row">
     <?php foreach($created as $create): ?>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 d-flex">
          <div class="card ">
            <img src="<?php echo htmlspecialchars($create['Immagine']);?>" class="card-img-top img-fluid" alt="immagine-evento">
            <div class="card-body">
              <h4 class="card-title"><?php echo htmlspecialchars($create['Titolo']);?></h4>
              <p class="card-text"><?php echo $create['Ospite']; ?>  |  <?php echo $create['Data']; ?>  |  <?php echo $create['Luogo']; ?> | <?php echo '<b> ' . $create['Costo_biglietto'] . '€</b>' ?></p>              
              <p class="card-text"><?php echo htmlspecialchars($create['Anteprima']);?></p>
            </div>
            <div class="card-footer">
              <a href="event.php?Codice=<?php echo $create['Codice'] ?>" class="btn btn-secondary mr-1 mt-1">Dettagli</a>
              <?php if(isset($_SESSION['organizer']) && ($_SESSION['organizer'] === 1)) : ?>
                <form class="form" style="float: center;" method="POST" action="includes/deleteEvent.inc.php?Codice=<?php echo $create['Codice'] ?>" > 
                  <button type="submit" name="delete" class="btn btn-danger mr-1 mt-1">Elimina <i class="fa fa-trash"></i></button>
                </form>
              <?php endif ?>
            </div>
        </div> 
       </div>
     <?php endforeach; ?>
    </div>
    </section>

   <?php 
        require_once("./templates/footer.php");
     ?>

</body>

</html>
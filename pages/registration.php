<!DOCTYPE html>
<html lang="it">
<head>
    <?php 
        require_once("./head.php");
     ?>

     <link href="../style/login.css" rel="stylesheet">

     <style type="text/css">
         span {
          color: red;
          font-size: small;
          padding-left: 5px;
          padding-top: 3px;
          font-style: italic;
      }


     </style>

</head>

<body class="text-center">

   <?php 
        require_once("./templates/header.php");
     ?>

    <!-- Form registrazione -->
    <form class="form-signin" action = "includes/signup.inc.php" method="POST" id="registrationForm" autocomplete="off">

        <img class="mb-4" src="../imgs/logo/logo.png" alt="" width="92" height="92">
        <legend class="h2 mb-3 font-weight-normal">Registrazione nuovo utente</legend>

        <?php 
        	if (isset($_GET['error'])) {
        		if ($_GET['error'] == 'emptyfields') {
        			echo '<span class=signuperror" > Compila tutti i campi! </span>';
        		} else if ($_GET['error'] == 'invalidemail') {
        			echo '<span class=signuperror"> Inserisci una mail valida! </span>';
        		} else if ($_GET['error'] == 'invalidpersonaldata') {
        			echo '<span class=signuperror" > I dati personali non devono contenere caratteri speciali o numeri! </span>';
        		} else if ($_GET['error'] == 'passwordnotmatching') {
        			echo '<span class=signuperror" > Le due password non corrispondono!</span>';
        		} else if ($_GET['error'] == 'alreadyregistered') {
        			echo '<span class=signuperror" > Sei già registrato!</span>';
        		}
        	}
         ?>

        <div class = "form-group">
          <input type="text" class="form-control" id="name" placeholder="Nome" name="name" autofocus>
          <p id = name-error></p>
        </div>
        <div class="form-group">
          <input type="text" class="form-control" id="surname" name="surname" placeholder="Cognome" >
          <p id = surname-error></p>
        </div>
        <div class="form-group">
          <input type="text" class="form-control" id="email" name="email" placeholder="Email">
          <p id = email-error></p>
        </div>        
        <div class="form-group">
          <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        </div>
        <div class="form-group">
          <input type="password" class="form-control" id="password-repeat" name="password-repeat" placeholder="Ripeti password">
        </div>
      
        <button class="btn btn-lg btn-primary btn-block" type="submit" id = "save-inputs" name = "save-inputs" value = "Submit" disabled>Registrati</button>

    </form>

   <?php 
        require_once("./templates/footer.php");
     ?>

</body>
    
     <!--Custom JS -->
    <script src = "./js/registrationFormValidation.js"></script>

</html>

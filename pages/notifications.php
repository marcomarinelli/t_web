<!DOCTYPE html>
<html lang="it">
<head>

<?php 
    require_once("./head.php");
     require_once("./dbConnect.php");
  ?>
<?php 

    if(isset($_SESSION['organizer']) && ($_SESSION['organizer'] == 1)) {

        $uid = $_SESSION['user_id'];

        $sql = "SELECT B_disponibili, Titolo FROM evento WHERE Codice_creatore = $uid " ;

        $result = mysqli_query($conn, $sql);

        if(mysqli_num_rows($result) > 0) {
          while($row = mysqli_fetch_assoc($result)) {

            $titolo = $row['Titolo'];

            if($row['B_disponibili'] == 0) {
            $_SESSION['notification'] = "<script type='text/javascript'>alert('Evento $titolo sold out!');</script>";
            echo $_SESSION['notification'];
            unset($_SESSION['notification']);
            }
          }
          
        } else {
          echo ' error ';
        }

      }

      if(isset($_SESSION['user_id']) && $_SESSION['organizer'] <> 1) {

        $uid = $_SESSION['user_id'];

        $sql = "SELECT evento.B_disponibili, evento.Titolo FROM evento INNER JOIN fattura ON $uid = fattura.Cod_cliente ";

        $result = mysqli_query($conn, $sql);

        if(mysqli_num_rows($result) > 0) {
          while($row = mysqli_fetch_assoc($result)) {

            $titolo = $row['Titolo'];

            if($row['B_disponibili'] == 0) {
            echo "<script type='text/javascript'>alert('Evento $titolo sold out!');</script>";
            }
          }
          
        } else {
          echo ' error ';
        }

      }
  
 ?>

</head>

<body>
    
    <?php 
        require_once("./templates/header.php");
    ?>


  <div class="pb-5" style="margin-top: 50px; min-height: 300px;">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 p-5 bg-light rounded shadow mb-5">

          <!-- Shopping cart table -->
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr class="rounded shadow-sm">
                  <th scope="col" class="border-0 bg-white ">
                    <div class="p-2 px-3 text-uppercase">Notifiche</div>
                  </th>
                </tr>
              </thead>
              <tbody>
                <th>
                  
                </th>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>      
    <?php 
        require_once("./templates/footer.php");
    ?>
    
  </body>

</html>